# pgcrypsi

## Getting started

### Installation Database

https://download.postgresql.org/pub/repos/yum/15/redhat/rhel-8-x86_64/

scp -r postgres/ [pfmadm@10.247.41.43](mailto:pfmadm@10.247.41.43):~

yum list installed "postgresql15*"
yum localinstall postgresql15-libs-15.5-2PGDG.rhel8.x86_64.rpm postgresql15-15.5-2PGDG.rhel8.x86_64.rpm postgresql15-server-15.5-2PGDG.rhel8.x86_64.rpm postgresql15-contrib-15.5-2PGDG.rhel8.x86_64.rpm -y

/var/lib/pgsql/15/data/pg_hba.conf

```
local   all             postgres                                trust   # -> first time to change password with \password 
local   all             all                                     peer
# IPv4 local connections:
host    all             all             127.0.0.1/32            scram-sha-256
# IPv6 local connections:
host    all             all             ::1/128                 scram-sha-256
# Allow replication connections from localhost, by a user with the
# replication privilege.
local   replication     all                                     peer
host    replication     all             127.0.0.1/32            scram-sha-256
host    replication     all             ::1/128                 scram-sha-256
hostnossl    all             all             0.0.0.0/0               md5
```

/var/lib/pgsql/15/data/postgresql.conf → change this

```
listen_addresses = '*'
```

psql

```
CREATE USER cpcadm WITH PASSWORD 'cpcadm@';
ALTER USER cpcadm Superuser;
CREATE DATABASE cpcuat;
```